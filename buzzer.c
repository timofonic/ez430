#include <msp430.h>
#include "display.h"
#include "timer.h"
#include <stdio.h>

#include "commonVar.h"
// *************************************************************************************************
// Author : Nicholas See
// Description : To initialise port pin 2.7 for the buzzer.
// *************************************************************************************************
void init_buzzer(void) {
	P2DIR |= BIT7;                     // P2.7 output , for buzzer

}


